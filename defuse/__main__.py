import sys
import logging

from .fs import DefuseFS
from .nodes import Directory, FileProxy


root = Directory({
    "proxied.rst": FileProxy(open("./README.rst", "rb"))
})

fs = DefuseFS("demofs", root)


if len(sys.argv) != 2:
    print("usage: %s <mountpoint>" % sys.argv[0])
    sys.exit(1)

logging.basicConfig(level=logging.DEBUG)
fs.run(sys.argv[1], foreground=True)

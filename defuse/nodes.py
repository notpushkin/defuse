from stat import S_IFDIR, S_IFREG


class Directory(dict):
    _perms = 0o555

    def getattr(self, fh=None):
        return {
            "st_mode": (S_IFDIR | self._perms),
        }

    def readdir(self, fh=None):
        yield "."
        yield ".."
        yield from self.keys()


class FileProxy:
    _perms = 0o444

    def __init__(self, fp, size=2**20):
        self.fp = fp
        self.size = size

    def getattr(self, fh=None):
        return {
            "st_mode": (S_IFREG | self._perms),
            "st_size": self.size,
        }

    def read(self, size, offset, fh=None):
        if self.fp.seekable():
            self.fp.seek(offset)

        return self.fp.read(size)

#!/usr/bin/env python
from __future__ import print_function, absolute_import, division

import logging

from collections import defaultdict
from errno import EROFS, ENOTSUP, EIO, ENOENT
from stat import S_IFDIR, S_IFLNK, S_IFREG
from sys import argv, exit
from os.path import split as split_path
from time import time

from fuse import FUSE, FuseOSError, Operations, LoggingMixIn


if not hasattr(__builtins__, "bytes"):
    bytes = str


def or_raise(val):
    if isinstance(val, Exception):
        raise val
    else:
        return val


class DefuseFS:
    """
    A simple `fuse.Operations`-like object that supports method delegation
    to its contents.
    """

    log = logging.getLogger("defusefs")
    defaults = {
        "access": 0,
        "chmod": FuseOSError(EROFS),
        "chown": FuseOSError(EROFS),
        "create": FuseOSError(EROFS),
        "destroy": None,
        "flush": 0,
        "fsync": 0,
        "fsyncdir": 0,
        "getattr": FuseOSError(ENOENT),
        "getxattr": FuseOSError(ENOTSUP),
        "init": None,
        "link": FuseOSError(EROFS),
        "mkdir": FuseOSError(EROFS),
        "mknod": FuseOSError(EROFS),
        "open": 0,
        "opendir": 0,
        "read": FuseOSError(EIO),
        "readdir": [".", ".."],
        "readlink": FuseOSError(ENOENT),
        "release": 0,
        "releasedir": 0,
        "removexattr": FuseOSError(ENOTSUP),
        "rename": FuseOSError(EROFS),
        "rmdir": FuseOSError(EROFS),
        "setxattr": FuseOSError(ENOTSUP),
        "statfs": {},
        "symlink": FuseOSError(EROFS),
        "truncate": FuseOSError(EROFS),
        "unlink": FuseOSError(EROFS),
        "utimens": 0,
        "write": FuseOSError(EROFS),
    }

    def __init__(self, name, root):
        self.name = name
        self.root = root

    def __getattr__(self, name):
        # HACK!
        if name in self.defaults:
            return lambda *a, **kw: self.defaults(name)

    def _get_file(self, path):
        segments = path.strip("/").split("/")
        cur_node = self.root
        for segment in segments:
            if segment == "":
                continue
            try:
                cur_node = cur_node[segment]
            except KeyError:
                raise FuseOSError(ENOENT)

        return cur_node

    def __call__(self, op, path, *args):
        self.log.debug("-> %s %s %s", op, path, repr(args))

        try:
            if op not in self.defaults:
                # Invalid operation
                raise FuseOSError(EFAULT)

            if op in ["link", "symlink"]:
                # TODO
                raise FuseOSError(EROFS)

            elif op in ["create", "mkdir", "mknod"]:
                _path, name = split_path(path)
                node = self._get_file(_path)
                if hasattr(node, op):
                    ret = getattr(node, op)(name, *args)
                    self.log.debug("<- %s %s %r", op, path, ret)
                    return ret
                else:
                    self.log.debug("<- %s %s (unsupported)", op, path)
                    return or_raise(self.defaults[op])

            else:
                node = self._get_file(path)
                if hasattr(node, op):
                    ret = getattr(node, op)(*args)
                    self.log.debug("<- %s %s %r", op, path, ret)
                    return ret
                else:
                    self.log.debug("<- %s %s (unsupported)", op, path)
                    return or_raise(self.defaults[op])

        except FuseOSError as e:
            self.log.debug("<! %s %s %r", op, path, e)
            raise

    def run(self, mountpoint, **kwargs):
        kwargs.setdefault("fsname", self.name)
        # kwargs.setdefault("debug", True)
        print("heeeeyyyy")
        return FUSE(self, mountpoint, **kwargs)

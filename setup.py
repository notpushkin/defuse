import os
import sys
from setuptools import setup

setup(
    name="defuse",
    version="0.0.0",
    author="Ale",
    author_email="hi@ale.rocks",
    description="A thin abstraction layer on top of FUSE",
    url="https://gitlab.com/iamale/defuse",
    packages=["defuse"],
    install_requires=[
        "fusepy==2.0.4",
    ],
)

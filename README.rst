Defuse
=======

A thin abstraction layer on top of `FUSE`_ and `fusepy`_. It is a work in progress.

.. _FUSE: https://github.com/libfuse/libfuse
.. _fusepy: https://github.com/terencehonles/fusepy

.. code-block:: Python

    from defuse.fs import DefuseFS
    from defuse.nodes import Directory, FileProxy


    root = Directory({
        "proxied.rst": FileProxy(open("./README.rst", "rb"))
    })

    fs = DefuseFS("demofs", root)

    if len(sys.argv) != 2:
        print("usage: %s <mountpoint>" % sys.argv[0])
        sys.exit(1)

    fs.run(sys.argv[1], foreground=True)

For a more realistic example, check out `yamusicfs`_, a Yandex.Music client.

.. _yamusicfs: https://gitlab.com/iamale/yamusicfs